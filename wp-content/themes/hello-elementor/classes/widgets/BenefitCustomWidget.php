<?php 

namespace WPC\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

if (!defined('ABSPATH')) exit;

class BenefitCustomWidget extends Widget_Base
{
    public function get_name()
    {
        return 'benefits';
    }

    public function get_title() 
    {
        return 'Benefícios';
    }

    public function get_icon()
    {
        return 'fas fa-box-open';
    }

    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
        wp_register_script( 'benefits', get_template_directory_uri() . '/assets/js/ElementorBenefitsCustomWidgets.js', [ 'elementor-frontend' ], '1.0.0', true );
        wp_register_style( 'benefits', get_template_directory_uri() . '/assets/css/ElementorBenefitsCustomWidgets.css');
    }
    

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls(){}

    protected function render()
    {
        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['teste_label-heading']
            ]
        )

        ?>
        <div class="category">
            
            <?php 

                $tax_query = [];
                if (isset($_GET['category'])) {
                    $category = $_GET['category'];
                    $tax_query = [
                        [
                            'taxonomy' => 'categorias',
                            'field' => 'slug',
                            'terms' => $category
                        ]
                    ];
                }

                $args = array(
                    'post_type' => 'beneficio',
                    'post_status' => 'publish',
                    'posts_per_page' => 4,
                    'tax_query' => $tax_query
                );

                $data = new \WP_Query( $args );

                $offset = count($data->posts);

                if ($offset > 3) {
                    array_pop($data->posts);
                }
            ?>

            <?php if (isset($data->posts) && is_array($data->posts)): ?>
                <div class="benefits">
                    <?php foreach($data->posts as $benefit): ?>
                        <?php
                            $logo = get_post_meta($benefit->ID, 'company_logo', true);
                            $image = get_post_meta($benefit->ID, 'benefit_image', true);
                            $company = get_post_meta($benefit->ID, 'company_name', true);
                            $company_url = get_post_meta($benefit->ID, 'company_url', true);
                            $resume = get_post_meta($benefit->ID, 'resume', true);
                            $discount = get_post_meta($benefit->ID, 'discount', true);
                        ?>

                        <div class="benefit">
                            <div class="benefit-image" style="background: url('<?php echo $image;?>');">
                                <div class="benefit-discount">
                                    <?php echo $discount;?>%<br>
                                    OFF
                                </div>
                            </div>
                            <div class="benefit-content">
                                <div class="company-logo"  style="background: url('<?php echo $logo;?>');">
                                </div>
                                <h4><?php echo $company;?></h4>
                                <h6><a href="<?php echo $company_url;?>" target="_blank"><?php echo $company_url;?></a></h6>
                                <?php echo $resume;?>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>

                <?php if ($offset > 3): ?>
                <div class="load-more" id="load-more" data-offset="<?php echo $offset;?>"><strong><i class="fas fa-plus-circle"></i> Carregar mais</strong></div>
                <?php endif; ?>

                <?php if (count($data->posts) == 0): ?>
                    <div class="text-center" style="color: #222;margin-top:50px;text-align:center;">
                        Nenhum resultado para esse filtro :(
                    </div>
                <?php endif;?>
                
            <?php endif;?>
        </div>
        <?php
    }

    public function get_script_depends() {
        return ['benefits'];
    }

    public function get_style_depends()
    {
        return ['benefits'];
    }

    protected function _content_template(){}
}