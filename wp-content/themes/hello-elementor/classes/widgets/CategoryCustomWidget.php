<?php 

namespace WPC\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

if (!defined('ABSPATH')) exit;

class CategoryCustomWidget extends Widget_Base
{
    public function get_name()
    {
        return 'benefit_categories';
    }

    public function get_title() 
    {
        return 'Categoria de benefícios';
    }

    public function get_icon()
    {
        return 'fa fa-star-o';
    }

    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
        wp_register_script( 'benefits', get_template_directory_uri() . '/assets/js/ElementorBenefitsCustomWidgets.js', [ 'elementor-frontend' ], '1.0.0', true );
        wp_register_style( 'benefits', get_template_directory_uri() . '/assets/css/ElementorBenefitsCustomWidgets.css');
    }
    

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls(){}

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['teste_label-heading']
            ]
        )

        ?>
        <div class="category">
            <?php 
                $terms = get_terms([
                    'taxonomy' => 'categorias',
                    'hide_empty' => false,
                ]);
            ?>

            <?php if (isset($terms) && is_array($terms)): ?>
                <div class="benefit-category">
                    <a href="<?php echo get_permalink();?>">
                        <div 
                            class="benefit-categories" 
                            style="border-color: #71cff5; color:#71cff5"
                            onmouseover="this.style.background='#71cff5'; this.style.color='#fff';"
                            onmouseout="this.style.background='#fff'; this.style.color='#71cff5';"
                            >
                            Todos
                        </div>
                    </a>
                    <?php foreach($terms as $term): ?>
                        <?php 
                            $category_color = 
                            isset(get_option( "taxonomy_{$term->term_id}")['benefits_category_color']) 
                            ? get_option( "taxonomy_{$term->term_id}")['benefits_category_color'] 
                            : '#000000'; 
                        ?>
                        <a href="?category=<?php echo $term->slug;?>">
                            <div 
                                class="benefit-categories" 
                                style="border-color: <?php echo $category_color; ?>; color: <?php echo $category_color;?>"
                                onmouseover="this.style.background='<?php echo $category_color;?>'; this.style.color='#fff';"
                                onmouseout="this.style.background='#fff'; this.style.color='<?php echo $category_color;?>';"
                            >
                                <?php echo $term->name;?>
                            </div>
                        </a>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
        <?php
    }

    public function get_script_depends() {
        return ['benefits'];
    }

    public function get_style_depends()
    {
        return ['benefits'];
    }

    protected function _content_template(){}
}