<?php 

class BenefitCustomTaxonomy {
    
    public function __construct()
    {
        add_action('registered_post_type', [$this, 'registerCategorySection']);
        add_action('registered_post_type', [$this, 'registerLocationSection']);
        add_action( 'categorias_add_form_fields', [$this, 'addNewFieldsToTaxonomyCategory']);
        add_action( 'categorias_edit_form_fields', [$this, 'editNewFieldsToTaxonomyCategory']);
        add_action( 'edited_categorias', [$this, 'saveTaxonomyCustomFields'], 10, 2 );  
        add_action( 'create_categorias', [$this, 'saveTaxonomyCustomFields'], 10, 2 );

    }

    /**
     * @author Pedro Henrique Guimarães
     * Register a custom taxonomy and add it on benefits submenu
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function registerCategorySection()
    {
        $labels = [
            'name' => 'Categorias',
            'singular_name' => 'Categorias',
            'search_items' => 'Procurar Categorias',
            'all_items' => 'Todas as Categorias',
            'parent_item' => 'Categorias pai',
            'parent_item_colon' => 'Categoria pai',
            'edit_item' => 'Editar Categoria',
            'update_item' => 'Atualizar Categoria',
            'add_new_item' => 'Adicionar Categoria',
            'new_item_name' => 'Agregar uma nova Categoria',
            'menu_name' => 'Categorias'
        ];

        // Now register the taxonomy
        register_taxonomy(
            'categorias',
            'beneficio',
            [
                'public' => true,
                'hierarchical' => false,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_rest' => true,
                'query_var' => true,
                'rewrite' => ['slug' => 'categorias']
            ]
        );
    }

    /**
     * @author Pedro Henrique Guimarães
     * Register a custom taxonomy and add it on benefits submenu
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function registerLocationSection()
    {
        $labels = [
            'name' => 'Locais',
            'singular_name' => 'Locais',
            'search_items' => 'Procurar Locais',
            'all_items' => 'Todas os Locais',
            'parent_item' => 'Local pai',
            'parent_item_colon' => 'Local pai',
            'edit_item' => 'Editar Local',
            'update_item' => 'Atualizar Local',
            'add_new_item' => 'Adicionar Local',
            'new_item_name' => 'Agregar uma nova Local',
            'menu_name' => 'Locais'
        ];

        // Now register the taxonomy
        register_taxonomy(
            'Locais',
            'beneficio',
            [
                'public' => true,
                'hierarchical' => false,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_rest' => true,
                'query_var' => true,
                'rewrite' => ['slug' => 'locais']
            ]
        );
    }

    /**
     * @author Pedro Henrique Guimarães
     * Add new custom fields for 'add taxonomy page'
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function addNewFieldsToTaxonomyCategory()
    {
        ?>
            <div class="form-field">
                <label for="term_meta[custom_term_meta]">Escolha a cor desta categoria</label>
                <input type="color" name="term_meta[benefits_category_color]" id="term_meta[benefits_category_color]" value="">
                <p class="description">Esta cor estará presente nas bordas e no texto da categoria</p>
            </div>
        <?php 
    }

    /**
     * @author Pedro Henrique Guimarães
     * Add new custom fields for 'edit taxonomy page'
     * 
     * @param object $term
     * @return void
     * 
     * @access public
     */
    public function EditNewFieldsToTaxonomyCategory($term)
    {

        $term_id = $term->term_id;

        $term_meta = get_option("taxonomy_$term_id");    

        ?>
            <tr class="form-field form-required term-name-wrap">
                <th scope="row"><label for="custom-field">Escolha a cor desta categoria</label></th>
                <td><input type="color" name="term_meta[benefits_category_color]" id="term_meta[benefits_category_color]" value="<?php echo esc_attr( $term_meta['benefits_category_color']);?>"></td>
            </tr>
        <?php 
    }

    /**
     * @author Pedro Henrique Guimarães
     * Save taxonomy custom fields data
     * 
     * @param int $term
     * @return void
     * 
     * @access public
     */
    public function saveTaxonomyCustomFields( $term_id ) 
    {
        if ( isset( $_POST['term_meta'] ) ) {
            $term_meta = get_option( "taxonomy_$term_id" );
            $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ) {
                if ( isset ( $_POST['term_meta'][$key] ) ) {
                    $term_meta[$key] = $_POST['term_meta'][$key];
                }
            }
            // Save the option array.
            update_option( "taxonomy_$term_id", $term_meta );
        }
    }  
}