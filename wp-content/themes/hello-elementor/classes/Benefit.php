<?php 

class Benefit {
    
    public function __construct()
    {
        add_action('init', [$this, 'registerBenefitSection']);
        add_action('registered_post_type', [$this, 'registerCategorySection']);
        add_action('registered_post_type', [$this, 'registerLocationSection']);
        add_action('admin_init', [$this, 'removeDefaultPostTypeFields']);
        add_action('admin_init', [$this, 'addBenefitsMetaBoxes']);
        add_action( 'admin_enqueue_scripts', [$this, 'includeScripts'] );
        add_action('save_post_beneficio', [$this, 'saveCustomFields']);

        //Ajax calls 
        add_action('wp_ajax_nopriv_load_more_benefits', [$this, 'loadMoreBenefits']); 
        add_action('wp_ajax_load_more_benefits', [$this, 'loadMoreBenefits']);
    }

    /**
     * @author Pedro Henrique Guimarães
     * Register a custom post type and add it on menu
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function registerBenefitSection()
    {
        $labels = array(
            'name' => _x('Benefícios', 'post type general name'),
            'singular_name' => _x('Benefício', 'post type singular name'),
            'add_new' => _x('Adicionar Novo', 'Novo Benefício'),
            'add_new_item' => __('Novo Benefício'),
            'edit_item' => __('Editar Benefício'),
            'new_item' => __('Novo Benefício'),
            'view_item' => __('Ver Benefício'),
            'search_items' => __('Procurar Benefícios'),
            'not_found' => __('Nenhum registro encontrado'),
            'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
            'menu_name' => 'Benefícios'
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'public_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 6,
            'menu_icon' => 'dashicons-clipboard',
        );
        register_post_type('beneficio', $args);
        flush_rewrite_rules();
    }

    /**
     * @author Pedro Henrique Guimarães
     * Register a custom taxonomy and add it on benefits submenu
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function registerCategorySection()
    {
        $labels = [
            'name' => 'Categorias',
            'singular_name' => 'Categorias',
            'search_items' => 'Procurar Categorias',
            'all_items' => 'Todas as Categorias',
            'parent_item' => 'Categorias pai',
            'parent_item_colon' => 'Categoria pai',
            'edit_item' => 'Editar Categoria',
            'update_item' => 'Atualizar Categoria',
            'add_new_item' => 'Adicionar Categoria',
            'new_item_name' => 'Agregar uma nova Categoria',
            'menu_name' => 'Categorias'
        ];

        // Now register the taxonomy
        register_taxonomy(
            'categorias',
            'beneficio',
            [
                'public' => true,
                'hierarchical' => false,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_rest' => true,
                'query_var' => true,
                'rewrite' => ['slug' => 'categorias']
            ]
        );
    }

    /**
     * @author Pedro Henrique Guimarães
     * Register a custom taxonomy and add it on benefits submenu
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function registerLocationSection()
    {
        $labels = [
            'name' => 'Locais',
            'singular_name' => 'Locais',
            'search_items' => 'Procurar Locais',
            'all_items' => 'Todas os Locais',
            'parent_item' => 'Local pai',
            'parent_item_colon' => 'Local pai',
            'edit_item' => 'Editar Local',
            'update_item' => 'Atualizar Local',
            'add_new_item' => 'Adicionar Local',
            'new_item_name' => 'Agregar uma nova Local',
            'menu_name' => 'Locais'
        ];

        // Now register the taxonomy
        register_taxonomy(
            'Locais',
            'beneficio',
            [
                'public' => true,
                'hierarchical' => false,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_rest' => true,
                'query_var' => true,
                'rewrite' => ['slug' => 'locais']
            ]
        );
    }

    /**
     * @author Pedro Henrique Guimarães
     * All custom post type has default fields, this method aims to remove all of them and provide a clean interface.
     * 
     * @param void
     * @return void
     * 
     * @access public
     */
    public function removeDefaultPostTypeFields() 
    {
        remove_post_type_support('beneficio', 'editor');
    }

    public function addBenefitsMetaBoxes()
    {
        add_meta_box( 'benefit_meta_box', 'Benefício', [$this, 'createBenefitMetaBox'], 'beneficio', 'normal', 'high' );
    }

    /**
     * @author Pedro Henrique Guimarães
     * Create custom fields for custom post type
     * 
     * @param object $benefit
     * @return void
     * 
     * @access public
     */
    public function createBenefitMetaBox($benefit) 
    {
        ?>
            <table width="100%">
                <tr>
                    <td>
                        <label for="custom-field"><strong>Nome da empresa</strong></label>
                        <input type="text" style="width:100%;" name="meta[company_name]" placeholder="Nome da empresa" value="<?php echo esc_html( get_post_meta( $benefit->ID, 'company_name', true ));?>" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="custom-field"><strong>URL da empresa</strong></label>
                        <input type="text" style="width:100%;" name="meta[company_url]" placeholder="URL da empresa" value="<?php echo esc_html( get_post_meta( $benefit->ID, 'company_url', true ));?>" required />
                    </td>
                </tr>
                <tr>
                    <?php $logo = get_post_meta($benefit->ID, 'company_logo', true);?>
                    <td>
                        <label for="custom-field"><strong>Logo da empresa</strong></label><br>
                        <a href="#" class="logo_upload_button button button-secondary"><?php _e('Upload Image'); ?></a>
                        <input type="hidden" name="meta[company_logo]" id="company_logo" value="<?php echo $logo; ?>" required readonly/>
                        <span class="company_logo"><?php echo $logo; ?></span>
                    </td>
                </tr>
                <tr>
                    <?php $image = get_post_meta($benefit->ID, 'benefit_image', true);?>
                    <td>
                        <label for="custom-field"><strong>Imagem ilustrativa do benefício</strong></label><br>
                        <a href="#" class="image_upload_button button button-secondary"><?php _e('Upload Image'); ?></a>
                        <input type="hidden" name="meta[benefit_image]" id="benefit_image" value="<?php echo $image; ?>" required readonly/>
                        <span class="benefit_image"><?php echo $image; ?></span>
                    </td>
                </tr>
            </table>
            <hr>
            <table width="100%">
                <tr>
                    <td>
                        <label for="custom-field"><strong>Resumo do benefício</strong></label>
                        <?php 
                            $text= get_post_meta( $benefit->ID, 'resume', true);
                            wp_editor( htmlspecialchars_decode($text), 'mettaabox_ID', $settings = array('textarea_name'=>'meta[resume]') );
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="custom-field"><strong>Desconto do benefício</strong></label><br>
                        <input type="number" style="width:50%;" name="meta[discount]" placeholder="Desconto (%)" value="<?php echo esc_html( get_post_meta( $benefit->ID, 'discount', true ));?>" required/>
                    </td>
                </tr>
            </table>
        <?php
    }

    /**
     * @author Pedro Henrique Guimarães
     * 
     * Save custom fields on as post_meta
     * 
     * @param int $benefit_id
     * @return void
     * 
     * @access public
     */
    public function saveCustomFields($benefit_id)
    {
        if (isset($_POST['meta'])) {
            foreach($_POST['meta'] as $key => $field) {
                if ($key == 'resume') {
                    update_post_meta(
                        $benefit_id,
                        $key,
                        $field
                    );
                } else {
                    update_post_meta(
                        $benefit_id,
                        $key,
                        $field
                    );
                }
            }
        }
    }

    public function loadMoreBenefits()
    {
        $offset = $_POST["offset"];
        $tax_query = [];
        if (isset($_GET['category'])) {
            $category = $_GET['category'];
            $tax_query = [
                [
                    'taxonomy' => 'categorias',
                    'field' => 'slug',
                    'terms' => $category
                ]
            ];
        }

        $args = array(
            'post_type' => 'beneficio',
            'post_status' => 'publish',
            'posts_per_page' => 3,
            'offset' => $offset,
            'tax_query' => $tax_query
        );

        $data = new \WP_Query( $args );

        ?>

        <?php if(!empty($data->posts)): ?>
            <?php foreach($data->posts as $benefit): ?>
                <?php
                    $logo = get_post_meta($benefit->ID, 'company_logo', true);
                    $image = get_post_meta($benefit->ID, 'benefit_image', true);
                    $company = get_post_meta($benefit->ID, 'company_name', true);
                    $company_url = get_post_meta($benefit->ID, 'company_url', true);
                    $resume = get_post_meta($benefit->ID, 'resume', true);
                    $discount = get_post_meta($benefit->ID, 'discount', true);
                ?>

                <div class="benefit">
                    <div class="benefit-image" style="background: url('<?php echo $image;?>');">
                        <div class="benefit-discount">
                            <?php echo $discount;?>%<br>
                            OFF
                        </div>
                    </div>
                    <div class="company-logo"  style="background: url('<?php echo $logo;?>');">
                    </div>
                    <div class="benefit-content">
                        <h4><?php echo $company;?></h4>
                        <h6><a href="<?php echo $company_url;?>" target="_blank"><?php echo $company_url;?></a></h6>
                        <?php echo $resume;?>
                    </div>
                </div>
            <?php endforeach;?>
        <?php else: ?>
            <?php echo json_encode(['over' => '1']); ?>
        <?php endif;?>

        <?php

         wp_reset_postdata();
         die();
    }

    /**
     * @author Pedro Henrique Guimarães
     * Include scripts
     * 
     * @param void
     * @return void 
     * 
     * @access public
     */
    public function includeScripts()
    {
        if ( ! did_action( 'wp_enqueue_media' ) ) {
            wp_enqueue_media();
        }
      
        wp_enqueue_script( 'benefits-scripts', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), null, false );
    }


}