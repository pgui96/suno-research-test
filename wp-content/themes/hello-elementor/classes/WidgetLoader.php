<?php

namespace WPC; 

class Widget_Loader {
     private static $_instance = null; 

     public static function instance()
     {
         if (is_null(self::$_instance)) {
             self::$_instance = new self();
         }

         return self::$_instance;
     }

     public function __construct()
     {
         add_action('elementor/widgets/widgets_registered', [$this, 'registerWidgets'], 99);
     }

     private function include_widgets_files()
     {
         require_once(__DIR__ . '/widgets/CategoryCustomWidget.php');
         require_once(__DIR__ . '/widgets/BenefitCustomWidget.php');
     }

     public function registerWidgets()
     {
         $this->include_widgets_files();

         \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Widgets\CategoryCustomWidget());
         \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Widgets\BenefitCustomWidget());
         
     }
}

Widget_Loader::instance();