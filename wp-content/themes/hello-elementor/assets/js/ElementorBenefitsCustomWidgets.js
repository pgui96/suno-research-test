jQuery(function($){
    let page = 1;
    $("#load-more").on("click", function(e) {
        $.ajax({
        url: $ajax_url,
        type: 'POST',
        data: {
        action: "load_more_benefits",
        offset: page * 3,
        category: jQuery('#load-more').data('category')
        },
        success: function(data) {
            page++;
            if (isJson(data)) {
                jQuery('#load-more').hide();
            } else {
                $('.benefits').append(data);
            }
        },
        error: function(data) {
            console.log(data);
        }
        });

    });
});

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}