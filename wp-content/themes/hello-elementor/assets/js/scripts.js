jQuery(function($){
    $('body').on('click', '.logo_upload_button', function(e){
        e.preventDefault();
  
        var button = $(this),
        uploader = wp.media({
            title: 'Logo da empresa',
            library : {
                uploadedTo : wp.media.view.settings.post.id,
                type : 'image'
            },
            button: {
                text: 'Use this image'
            },
            multiple: false
        }).on('select', function() {
            var attachment = uploader.state().get('selection').first().toJSON();
            $('#company_logo').val(attachment.url);
            $('.company_logo').text(attachment.url);
        })
        .open();
    });

    $('body').on('click', '.image_upload_button', function(e){
        e.preventDefault();
  
        var button = $(this),
        uploader = wp.media({
            title: 'Imagem ilustrativa',
            library : {
                uploadedTo : wp.media.view.settings.post.id,
                type : 'image'
            },
            button: {
                text: 'Use this image'
            },
            multiple: false
        }).on('select', function() {
            var attachment = uploader.state().get('selection').first().toJSON();
            $('#benefit_image').val(attachment.url);
            $('.benefit_image').text(attachment.url);
        })
        .open();
    });
});